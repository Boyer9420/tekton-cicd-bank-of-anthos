# Overview

This is a project that contains the Tekton objects for building Bank of Anthos.

## Installed elements

Project `Config Sync Setup` in this group has the ability to install the core Operator and components to Tekton. Please make sure you install these first. If you have run the install, but did not choose to install Tekton at the time, you can re-run using:

```bash
# NOTE: From the config-sync-setup project
export INSTALL_TEKTON="true"
export EXPOSE_TEKTON_DASHBOARD="true"
./install.sh tekton
```

## CICD

1. TriggerTemplate - Pipeline, image repository, etc
1. TriggerBinding - Branch triggered from, git-sha, commit message and user(s)
1. EventListener - git push, git tag
